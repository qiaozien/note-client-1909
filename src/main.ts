import { createApp } from "vue"
import { createPinia } from "pinia"
import App from "./App.vue"
import router from "./router"
import uiPlugin from "@/plugin/ui.registry"
import baseComponent from "@/plugin/baseComponet.registry"
import "@/styles/theme.css"
import "@/styles/common.less"

createApp(App)
  .use(createPinia())
  .use(router)
  .use(uiPlugin)
  .use(baseComponent)
  .mount("#app")
